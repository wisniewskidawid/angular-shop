import {Component} from '@angular/core';
@Component({
    selector: 'pm-products',
    templateUrl: './product-list.component.html'
})
export class ProductListComponent {
    pageTitle: string = 'Lista fantów';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    listFilter: string = 'wózeczek';
    products: any[] = [
        {
            "productId": 1,
            "productName": "Grabki",
            "productCode": "OGR-0011",
            "releaseDate": "19 Marca, 2016",
            "description": "Grabki do liści na kijku.",
            "price": 59.99,
            "starRating": 3.2,
            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
        },
        {
            "productId": 2,
            "productName": "Wózeczek",
            "productCode": "OGR-0023",
            "releaseDate": "18 Marca, 2016",
            "description": "50 litrowy wózeczek na 2 kółkach",
            "price": 122.99,
            "starRating": 4.2,
            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
        },
        {
            "productId": 5,
            "productName": "Młotek",
            "productCode": "TBX-0048",
            "releaseDate": "21 Maja, 2016",
            "description": "Młotek jak to młotek",
            "price": 9.99,
            "starRating": 4.8,
            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png"
        },
    ];

    toggleImage(): void{
        this.showImage = !this.showImage;
    }
}